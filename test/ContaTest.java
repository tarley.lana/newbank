/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import jdk.nashorn.internal.ir.annotations.Ignore;
import newbank.Conta;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 *
 * @author Tarley
 */
public class ContaTest {
    
    public ContaTest() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUpMethod() throws Exception {
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
    }
    
    @Test
    public void testeDepositar(){
        Conta c = new Conta("2299292929");
        Double saldoAtual = c.depositar(10.0);
        
        Assert.assertEquals(saldoAtual, 20.0, "Opa! depositar tá ruim");
        
        
        saldoAtual = c.depositar(-10.0);
        Assert.assertEquals(saldoAtual, 20.0, "Opa! depositar tá ruim");
        
    }
    
    @Test
    public void testSacar() {
        Conta c = new Conta("78888999000");
        c.depositar(20.0);
        Double saldoAtual = c.sacar(15.0);
        Assert.assertEquals(saldoAtual, 15.0);
       
    }
    
    @Test
    public void testSacarNegativo() {
        Conta c = new Conta("78888999000");
        c.depositar(20.0);
        Double saldoAtual = c.sacar(-15.0);
        Assert.assertEquals(saldoAtual, 30.0);
       
    }
    
    @Test(expectedExceptions 
            = IllegalArgumentException.class)
    public void testSacarValorMaiorSaldo() {
        Conta c = new Conta("78888999000");
        c.depositar(20.0);
        Double saldoAtual = c.sacar(31.0);
       
    }
    
    
}
