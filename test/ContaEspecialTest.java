/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import newbank.Conta;
import newbank.ContaEspecial;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

import org.testng.annotations.Test;
/**
 *
 * @author Tarley
 */
public class ContaEspecialTest {
    
    public ContaEspecialTest() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUpMethod() throws Exception {
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
    }
    
    @Test
    public void sacarAlemLimite() {
        Conta ce = new ContaEspecial("11111111111");
        ((ContaEspecial) ce).setLimite(50.0);
        Double saldoAtual = ce.sacar(11.0);
        Assert.assertEquals(saldoAtual, -1.0, "Falha limite");
    }
}
