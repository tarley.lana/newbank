/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newbank;

/**
 *
 * @author Tarley
 */
public class Conta {
    private String numero;
    protected Double saldo;

    public Conta(String numero) {
        setNumero(numero);
        this.saldo = 10.0;
    }
    
    public Double depositar(double valor) {
        if(valor <= 0)
            return saldo;
        
        saldo += valor;
        return saldo;
    }
    
    public Double sacar(double valor) {
        if(valor <= 0)
            return saldo;
        
        if(valor > saldo)
            throw new IllegalArgumentException("Saldo insu...");
        
        saldo -= valor;
        return saldo;
    }
    
    /**
     * @return the numero
     */
    public String getNumero() {
        return numero;
    }

    /**
     * @param numero the numero to set
     */
    private void setNumero(String num) {
        if(num == null || num.trim().equals("") 
                || num.length() < 10)
            throw new IllegalArgumentException();
        
        numero = num;
    }

    /**
     * @return the saldo
     */
    public Double getSaldo() {
        return saldo;
    }
    
    public void finalize() {
                 
    }
}
