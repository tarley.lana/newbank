/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newbank;

/**
 *
 * @author Tarley
 */
public class ContaEspecial extends Conta {
    private Double limite;

    public ContaEspecial(String numero) {
        super(numero);
    }

    /**
     * @return the limite
     */
    public Double getLimite() {
        return limite;
    }

    /**
     * @param limite the limite to set
     */
    public void setLimite(Double limite) {
        this.limite = limite;
    }

    @Override
    public Double sacar(double valor) {
        if(valor <=0)
            return super.getSaldo();
        
        if(valor > this.getLimite() + super.getSaldo())
            throw new IllegalArgumentException();
        
        saldo -= valor;
        
        return saldo;
    }
    
    
}
